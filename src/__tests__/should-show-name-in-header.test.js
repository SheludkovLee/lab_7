import React from 'react';
import { render } from '@testing-library/react';
import { it, expect } from '@jest/globals';
import { Header } from '../components';

it(`Header должен отображать имя пользователя`, () => {
  const name = 'John Doe';

  const { container } = render(<Header name={name} />);

  expect(container).toHaveTextContent(name);
});
